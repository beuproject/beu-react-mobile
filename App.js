import React from 'react';
import {
  StyleSheet,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  useColorScheme,
  View,
  TouchableOpacity,
} from 'react-native';

const image = require('./assets/images/background.jpg');
const logo = require('./assets/images/logo.png');

export default function App() {
  return (
      <View style={styles.container}>
        <ImageBackground source={image} style={styles.imgBackground}>
        <View style={styles.bottom}>
          <Image source={logo}></Image>
        </View>
        <View style={styles.bottom}>
          <TouchableOpacity style={styles.loginBtn}>
            <Text style={styles.loginText}>Log In</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.signupBtn}>
            <Text style={styles.registerText}>Sign Up</Text>
          </TouchableOpacity>
        </View>
        <StatusBar translucent backgroundColor="transparent" barStyle="light-content" />
        </ImageBackground>
      </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  imgBackground: {
      width: '100%',
      height: '100%',
      flex: 1 
  },
  loginBtn: {
    width:"90%",
    borderRadius:20,
    height:48,
    alignItems:"center",
    justifyContent:"center",
    marginTop:40,
    backgroundColor:"#0077B6",
  },
  signupBtn: {
    width:"90%",
    borderRadius:20,
    height:48,
    alignItems:"center",
    justifyContent:"center",
    marginTop:10,
    backgroundColor:"transparent",
    borderWidth: 1,
    borderColor: "#fff"
  },
  loginText: {
    color: "white"
  },
  registerText: {
    color: "white"
  },
  bottom: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginBottom: 20,
    width: "100%"
  }
});
